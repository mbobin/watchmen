require File.expand_path '../spec_helper.rb', __FILE__

describe "Application" do
  context "without params" do
    before do
      post "/api"
    end

    it { expect(last_response).to be_unprocessable }
    it { expect(last_response.content_type).to include("json") }
    it { expect(last_response.body).to include("is missing") }
  end

  context "with success message" do
    let(:message) do
      "time openvpn[1234]: 37.235.1.174:22 [bob] Peer Connection Initiated with [AF_INET]37.235.1.174:22"
    end

    before do
      stub_request(:get, /http:\/\/api\.ipstack\.com\/.*/)
        .to_return(status: 200, body: '{ "longitude": 1, "latitude": 1 }', headers: {})

      stub_request(:post, /.*/)
        .with(body: "geo_vpn_success,geohash=s00twy01mtw0,username=bob,port=22,ip=37.235.1.174 value=1i")
        .to_return(status: 204, body: "", headers: {})

      post "/api", { payload: message }
    end

    it { expect(last_response).to be_created }
    it { expect(last_response.content_type).to include("json") }
    it { expect(last_response.body).to be_empty }
  end

  context "with error message" do
    let(:message) do
      "time openvpn[1234]: 37.235.1.174:22 TLS Auth Error:"
    end

    before do
      stub_request(:get, /http:\/\/api\.ipstack\.com\/.*/)
        .to_return(status: 200, body: '{ "longitude": 1, "latitude": 1 }', headers: {})

      stub_request(:post, /.*/)
        .with(body: "geo_vpn_fail,geohash=s00twy01mtw0,username=undefined,port=22,ip=37.235.1.174 value=1i")
        .to_return(status: 204, body: "", headers: {})

      post "/api", { payload: message }
    end

    it { expect(last_response).to be_created }
    it { expect(last_response.content_type).to include("json") }
    it { expect(last_response.body).to be_empty }
  end
end

