#!/usr/bin/env bash

set -euxo pipefail

DIRECTORY="provisioning"
ARCHIVE="${DIRECTORY}.tar.gz"
DATA_URL="https://gitlab.com/mbobin/watchmen-grafana-dashboards/-/archive/master/watchmen-grafana-dashboards-master.tar.gz"

if [ -d "$DIRECTORY" ]; then
  echo "Removing existing data from ${DIRECTORY}"
  rm -rf $DIRECTORY
fi

wget $DATA_URL -O $ARCHIVE
mkdir -p $DIRECTORY
tar -xzvf $ARCHIVE --strip 1 --directory $DIRECTORY
rm $ARCHIVE
