require 'bundler'
Bundler.require(:default)

require_relative 'config'
require_relative 'geo_ip'
require_relative 'connection_schema'
require_relative 'influx_data_message'
require_relative 'influx_ingester'
require_relative 'messages/base'
require_relative 'messages/default'
require_relative 'messages/openvpn/success'
require_relative 'messages/openvpn/fail'

class DbContainer
  extend Dry::Container::Mixin
end

DbContainer.register(:influxdb) { InfluxDB::Client.new(Config.database.name, Config.database.credentials.to_h) }
DbContainer.resolve(:influxdb).create_database

MESSAGE_FORMATS = [
  Messages::OpenVPN::Success,
  Messages::OpenVPN::Fail::CertificateRevoked,
  Messages::OpenVPN::Fail::AuthError,
  Messages::Default
]

configure do
  set :server, :puma
  set :port, 4000
  set :log_requests, true
end

post "/api" do
  data = MESSAGE_FORMATS
    .map { |klass| klass.new(params["payload"]) }
    .detect { |obj| obj.matches? }

  schema = ConnectionSchema.call(data.to_h)
  content_type :json

  if schema.success?
    InfluxIngester.new(params: schema, metric_name: data.metric_name).call
    status :created
  else
    status :unprocessable_entity
    body schema.errors.to_hash.to_json
  end
end
