module Messages
  class Default < Base
    def matches?
      true
    end

    def to_h
      {}
    end
  end
end

