module Messages
  class Base
    attr_reader :message

    def initialize(message)
      @message = message.to_s
    end

    def matches?
      NotImplementedError
    end

    def to_h
      NotImplementedError
    end

    def metric_name
      raise NotImplementedError
    end
  end
end

