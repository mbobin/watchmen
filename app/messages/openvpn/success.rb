module Messages
  class OpenVPN
    class Success < Base
      def matches?
        @captures ||= message.match(regexp)
      end

      def to_h
        @captures&.named_captures.to_h
      end

      def metric_name
        "geo_vpn_success"
      end

      private

      def regexp
        ip_block = /\d{,2}|1\d{2}|2[0-4]\d|25[0-5]/
        ip = /(?<ip>#{ip_block}\.#{ip_block}\.#{ip_block}\.#{ip_block})/
        extra = / Peer Connection Initiated with \[AF_INET\]/
        regex = /.* openvpn\[\d+\]: #{ip}:(?<port>\d+) \[(?<username>[a-zA-Z\-\_]*)\]#{extra}(\k<ip>):(\k<port>)\z/
      end
    end
  end
end
