module Messages
  class OpenVPN
    module Fail
      class AuthError < Base
        AUTH_ERRORS = [
          /TLS Auth Error:/,
          /VERIFY ERROR:/,
          /TLS Error: TLS handshake failed/,
          /TLS Error: cannot locate HMAC in incoming packet from/,
        ]

        def matches?
          message.match(/TLS Auth Error:/)
        end

        def to_h
          @captures ||= extract_captures

          return @captures if @captures.empty?
          @captures["username"] ||= "undefined"

          @captures
        end

        def metric_name
          "geo_vpn_fail"
        end

        private

        def extract_captures
          message.match(regexp)&.named_captures.to_h
        end

        def regexp
          ip_block = /\d{,2}|1\d{2}|2[0-4]\d|25[0-5]/
          ip = /(?<ip>#{ip_block}\.#{ip_block}\.#{ip_block}\.#{ip_block})/
          auth = Regexp.union(AUTH_ERRORS)
          /.* openvpn\[\d+\]: #{ip}:(?<port>\d+) #{auth}.*\z/
        end
      end
    end
  end
end
