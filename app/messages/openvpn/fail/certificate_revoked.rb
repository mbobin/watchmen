module Messages
  class OpenVPN
    module Fail
      class CertificateRevoked < Base
        def matches?
          message.match(/VERIFY ERROR:.*error=certificate revoked: CN/)
        end

        def to_h
          @captures ||= message.match(regexp)

          @captures&.named_captures.to_h
        end

        def metric_name
          "geo_vpn_fail"
        end

        private

        def regexp
          ip_block = /\d{,2}|1\d{2}|2[0-4]\d|25[0-5]/
          ip = /(?<ip>#{ip_block}\.#{ip_block}\.#{ip_block}\.#{ip_block})/
          extra = /.*certificate revoked: CN=(?<username>[a-zA-Z\-\_]*)/
          regex = /.* openvpn\[\d+\]: #{ip}:(?<port>\d+) VERIFY ERROR: #{extra}\z/
        end
      end
    end
  end
end
